import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mail_shot/signup.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'home.dart';

class SignIN extends StatefulWidget {
  @override
  _SignINState createState() => _SignINState();
}

class _SignINState extends State<SignIN> {
  final _auth = FirebaseAuth.instance;
  String email, password;

  bool showSpinner = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: showSpinner,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text("Sign In"),
                Text("Enter your email:"),
                Expanded(
                  flex: 2,
                  child: Center(
                    child: TextField(
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        email = value; //Taking Email
                      },
                    ),
                  ),
                ),
                Text("Password:"),
                Expanded(
                  flex: 2,
                  child: TextField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    onChanged: (v) {
                      password = v; //taking password
                    },
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: IconButton(
                    icon: Icon(Icons.send),
                    iconSize: 60,
                    onPressed: () {
                      setState(() {
                        showSpinner=true;
                      });
                      onSigningIn();
                    },
                  ),
                ),
                TextButton(onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => SignUp()));
                }, child: Text("Don't have an account?"))
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onSigningIn() async {
    try {
      final newUser = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      if (newUser != null) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Home()));
      }
    } catch (e) {
      print(e);
    }
    setState(() {
      showSpinner=false;
    });
  }
}