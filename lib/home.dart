import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'compose.dart';
import 'history_page.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currenttab = 0;
  final List<Widget> screens = [
    History_page(),
    Compose(),
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentscreen = Dashboard();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentscreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          setState(() {
            Navigator.push(context, MaterialPageRoute(builder: (context)=>Compose()));
          });
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: MaterialButton(

                  onPressed: () {
                    setState(() {
                      currentscreen = Dashboard();
                      currenttab = 0;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.home,
                        color: currenttab == 0? Colors.blue : Colors.grey,
                      ),
                      Text('Home')

                    ],
                  ),
                ),
              ),
              Expanded(
                child: MaterialButton(

                  onPressed: () {
                    setState(() {
                      currentscreen = History_page();
                      currenttab = 1;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.history,
                        color: currenttab == 1? Colors.blue : Colors.grey,
                      ),
                      Text('History mails')
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}


Widget appbar(var title,var icon,BuildContext context){
  return AppBar(
    title: Text(title),
    centerTitle: true,
    actions: [
      IconButton(
        icon: Icon(icon),
        onPressed: () {
          if(icon == Icons.info_outline){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Intro_pages()),
            );
          }
          else{
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => settings()),
            );
          }
        },
      ),
    ],
    leading: IconButton(
      icon: Icon(Icons.account_circle_outlined,
      ),
      onPressed: () {

      },
    ),
  );
}